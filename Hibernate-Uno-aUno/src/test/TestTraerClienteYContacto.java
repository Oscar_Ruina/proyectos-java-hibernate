package test;

import dao.ClienteDao;
import dao.HibernateUtil;
import datos.Cliente;

public class TestTraerClienteYContacto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClienteDao dao = new ClienteDao();
		long idCliente = 4;
		Cliente c = dao.traerCliente(idCliente);
		System.out.println("\ntraer cliente y contacto\n" + c + "\n" + c.getContacto());
		HibernateUtil.getSessionFactory().close();

	}

}
