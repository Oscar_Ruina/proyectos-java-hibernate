package test;

import java.util.GregorianCalendar;
import negocio.ClienteABM;
import dao.HibernateUtil;

public class TestAgregarCliente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String apellido = "Ruina";
		String nombre = "Oscar";
		int documento = 35639606;
		GregorianCalendar fechaDeNacimiento = new GregorianCalendar(1990,11,04);
		ClienteABM abm = new ClienteABM();
		try {
			long ultimoIdCliente = abm.agregar(apellido, nombre, documento, fechaDeNacimiento);
			System.out.println(ultimoIdCliente);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
			HibernateUtil.getSessionFactory().close();
		}

	}

}
