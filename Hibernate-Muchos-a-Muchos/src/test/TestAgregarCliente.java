package test;

import datos.Cliente;
import negocio.ClienteABM;
import dao.HibernateUtil;
import java.time.*;

public class TestAgregarCliente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			ClienteABM c = ClienteABM.getInstancia();
			int id = c.agregar(new Cliente("Ruina","Oscar",35639606,LocalDate.of(1990, 11, 04)));
			System.out.println(id);
			System.out.println(c.traerCliente(35639606));
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
			HibernateUtil.getSessionFactory().close();
		}

	}

}
